# AsiLoader

## Install

just copy file VorbisFile.dll into your game directory

## Build

```bash
dub build -brelease
```

#### Cross-compilation

For cross-compilation your need use **LDC2**
```bash
dub build -brelease --compiler=ldc2 --arch=i686-pc-windows-msvc
```

### [Also I Express my gratitude to the group of testers who helped to find and fix the bugs of the project.](TESTERS.md)
