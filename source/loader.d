import std.string : empty, toStringz;
import core.sys.windows.windows, core.sys.windows.dll, core.runtime;
import vorbisfile;
import config;
import lib;
import std.format : format;
import llmo;

public __gshared static stConfig!"VorbisFile.ini" ini;

void hook_mainloop()
{
	ubyte[] hook;
	hook ~= saveRegisters;
	uint hook_call = hook.length;
	hook ~= callToNull;
	hook ~= loadRegisters;
	hook ~= readMemory(0x748DA3, 6);
	hook ~= callReturn;

	fixNull(cast(ubyte*)(cast(uint)hook.ptr + hook_call), &mainloop);

	setMemoryProtection(hook.ptr, hook.length);
	simpleCallHook(0x748DA3, cast(void*)hook.ptr, 6);
}

void hook_GetStartupInfoA()
{
	ubyte[] hook;
	hook ~= saveRegisters;
	uint hook_call = hook.length;
	hook ~= callToNull;
	hook ~= loadRegisters;
	hook ~= readMemory(0x8246BF, 6);
	hook ~= callReturn;

	fixNull(cast(ubyte*)(cast(uint)hook.ptr + hook_call), &GetStartupInfoA);

	setMemoryProtection(hook.ptr, hook.length);
	simpleCallHook(0x8246BF, cast(void*)hook.ptr, 6);
}

void hook_crtMain()
{
	ubyte[] hook;
	hook ~= saveRegisters;
	uint hook_call = hook.length;
	hook ~= callToNull;
	hook ~= loadRegisters;
	hook ~= readMemory(0x824570, 7);
	uint hook_jump = hook.length;
	hook ~= jumpToNull;

	fixNull(cast(ubyte*)(cast(uint)hook.ptr + hook_call), &crtMain);
	fixNull(cast(ubyte*)(cast(uint)hook.ptr + hook_jump), cast(void*)(0x824570 + 7));

	setMemoryProtection(hook.ptr, hook.length);
	simpleJmpHook(0x824570, cast(void*)hook.ptr, 7);
}
__gshared ubyte[5] kernel32_ExitProcess_bak;
void hook_ExitProcess()
{
	auto kernel32 = GetModuleHandleA("kernel32.dll");
	uint kernel32_ExitProcess = cast(uint)GetProcAddress(kernel32, "ExitProcess");
	kernel32_ExitProcess_bak = readMemory(kernel32_ExitProcess, 5);
	ubyte[] hook;
	hook ~= saveRegisters;
	uint hook_call = hook.length;
	hook ~= callToNull;
	hook ~= loadRegisters;
	uint hook_jump = hook.length;
	hook ~= jumpToNull;

	fixNull(cast(ubyte*)(cast(uint)hook.ptr + hook_call), &ExitProcess);
	fixNull(cast(ubyte*)(cast(uint)hook.ptr + hook_jump), cast(void*)(kernel32_ExitProcess));

	setMemoryProtection(hook.ptr, hook.length);
	simpleJmpHook(kernel32_ExitProcess, cast(void*)hook.ptr, 5);
}

void init(){
	hook_crtMain();
 	hook_GetStartupInfoA();
 	hook_mainloop();
	ini.load;
	InitLibProxy(ini.proxy);
}
extern(Windows) {
	BOOL DllMain(HINSTANCE hInstance, ULONG ulReason, LPVOID pvReserved)
	{
		switch (ulReason)
		{
			case DLL_PROCESS_ATTACH:
				Runtime.initialize();
				init();
				dll_process_attach(hInstance, true);
				break;
			case DLL_PROCESS_DETACH:
				ini.save;
				Runtime.terminate();
				dll_process_detach(hInstance, true);
				break;
			case DLL_THREAD_ATTACH:
				dll_thread_attach(true, true);
				break;
			case DLL_THREAD_DETACH:
				dll_thread_detach(true, true);
				break;
			
			default:
				break;
		}
		return true;
	}

	void crtMain()
	{
		foreach(plugin; ini.preload)
			LoadLibraryA(plugin.toStringz);
		
		if (ini.asiLoader.enable && ini.asiLoader.loadState == eLoadState.crtMain)
			loadAsi();
		
		if (ini.dllLoader.enable && ini.dllLoader.loadState == eLoadState.crtMain){
			if (ini.devMode.enable)
				loadDll(eLoadType.devCrtMainLoad);
			else loadDll(eLoadType.crtMainLoad);
		}
	}
	
	void GetStartupInfoA(){
		__gshared static bool asiLoaded = false;
		__gshared static bool dllLoaded = false;
		
		if (!asiLoaded && ini.asiLoader.enable && ini.asiLoader.loadState == eLoadState.GetStartupInfoA){
			asiLoaded = true;
			loadAsi();
		}
		
		if (!dllLoaded && ini.dllLoader.enable && ini.dllLoader.loadState == eLoadState.GetStartupInfoA){
			dllLoaded = true;
			if (ini.devMode.enable)
				loadDll(eLoadType.devGetStartupInfoALoad);
			else loadDll(eLoadType.GetStartupInfoALoad);
		}
	}

	void mainloop()
	{
		__gshared static bool exitHook = false;
		__gshared static bool asiLoaded = false;
		__gshared static bool dllLoaded = false;
		
		if (!asiLoaded && ini.asiLoader.enable && ini.asiLoader.loadState == eLoadState.mainloop){
			asiLoaded = true;
			loadAsi();
		}
		
		if (!dllLoaded && ini.dllLoader.enable && ini.dllLoader.loadState == eLoadState.mainloop){
			dllLoaded = true;
			if (ini.devMode.enable)
				loadDll(eLoadType.devMainloopLoad);
			else loadDll(eLoadType.mainloopLoad);
		}
		
		if (!exitHook){
			exitHook = true;
			hook_ExitProcess();
		}
		
		if (ini.devMode.enable)
			updateLibraryes();
	}
	
	void ExitProcess()
	{
		unloadAll;
		auto kernel32 = GetModuleHandleA("kernel32.dll");
		uint kernel32_ExitProcess = cast(uint)GetProcAddress(kernel32, "ExitProcess");
		writeMemory(kernel32_ExitProcess, kernel32_ExitProcess_bak);
	}
}
