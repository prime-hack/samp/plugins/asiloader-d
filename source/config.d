module config;

import std.file : exists, isFile;
import inifiled;

@INI("AsiLoader settings", "AsiLoader")
struct stAsiLoader{
	@INI("Enable builtin asi loader") bool enable = true;
	@INI("State to load asi. Available: crtMain, GetStartupInfoA, mainloop") eLoadState loadState = eLoadState.GetStartupInfoA;
	@INI("Don't load this asi. Example: exlude=\"CLEO.asi,SAMPFUNCS.asi\"") string[] exclude;
};

@INI("DllLoader settings", "DllLoader")
struct stDllLoader{
	@INI("Enable loading dll's from directory 'plugins'") bool enable = true;
	@INI("Directory with dll-plugins") string path = "plugins";
	@INI("State to load dll. Available: crtMain, GetStartupInfoA, mainloop") eLoadState loadState = eLoadState.crtMain;
	@INI("Don't load this libraries. Example: exlude=\"test.dll,plugin.dll\"") string[] exclude;
};

@INI("Developer settings", "DevMode")
struct stDevMode{
	@INI("Developer mode. Autoreload plugins from disk") bool enable = false;
	@INI("Unload deleted plugins") bool unloadDeleted = false;
	@INI("Load new plugins") bool loadNew = false;
};

enum eLoadState : byte{
	crtMain = 0,
	GetStartupInfoA,
	mainloop
};

@INI("Main settings", "Config")
struct stConfig(string inifile){
	void load(){
		if (inifile.exists && inifile.isFile)
			this.readINIFile(inifile);
	}
	void save(){
		this.writeINIFile(inifile);
	}
	
	@INI("Proxy library for asi loader") string proxy;
	@INI("Preload libraryes. Example: preload=\"samp.dll,plugin.dll\"") string[] preload;
	@INI stDevMode devMode;
	@INI stAsiLoader asiLoader;
	@INI stDllLoader dllLoader;
};
