module lib;

import core.sys.windows.windows : LoadLibraryA, FreeLibrary, GetProcAddress, GetTickCount;
import std.uni : asLowerCase;
import std.file : exists, isDir, read, getTimes, mkdir, dirEntries, getcwd, SpanMode, tempDir, mkdirRecurse, copy, remove;
import std.path : extension, baseName, buildPath, dirName;
import std.process : thisProcessID;
import std.conv : to;
import std.datetime : SysTime;
import std.string : toStringz;
import std.algorithm.comparison : equal;
import core.memory : pureFree;
import loader : ini;
import config;
import fswatch;

alias cb_load = extern (C) void function(eLoadType) @nogc nothrow pure @safe;
alias cb_unload = extern (C) void function() @nogc nothrow pure @safe;

private __gshared static stLibInfo[] asiPlugins;
private __gshared static stLibInfo[] dllPlugins;

public void updateLibraryes(){
	__gshared static bool initWatchers;
	__gshared static FileWatch* asiWatcher, dllWatcher;
	if (!initWatchers){
		asiWatcher = new FileWatch("./");
		dllWatcher = new FileWatch(ini.dllLoader.path ~ "/");
		initWatchers = true;
	}

	static auto update = (FileWatch* watch, bool asi){
		static auto unloadByPath = (ref string path, stLibInfo[] libs){
			foreach (ref stLibInfo lib; libs){
				if (lib.library.baseName == path.baseName){
					if (lib.library.extension.asLowerCase.equal(".dll"))
						lib.callUnload;
					lib.libUnload;
					break;
				}
			}
		};
		foreach(event; watch.getEvents()){
			if ((event.type == FileChangeEventType.remove || event.type == FileChangeEventType.rename) && ini.devMode.unloadDeleted){
				if (!event.path.extension.asLowerCase.equal(asi ? ".asi" : ".dll"))
					continue;
				if (asi)
					unloadByPath(event.path, asiPlugins);
				else unloadByPath(event.path, dllPlugins);
			}
			else if ((event.type == FileChangeEventType.create || event.type == FileChangeEventType.rename) && ini.devMode.loadNew){
				if (event.type == FileChangeEventType.create && !event.path.extension.asLowerCase.equal(asi ? ".asi" : ".dll"))
					continue;
				if (event.type == FileChangeEventType.rename)
					event.newPath.loadPlugin;
				else event.path.loadPlugin;
				continue;
			}
			else if (event.type == FileChangeEventType.modify){
				if (!event.path.extension.asLowerCase.equal(asi ? ".asi" : ".dll"))
					continue;
				if (asi)
					unloadByPath(event.path, asiPlugins);
				else unloadByPath(event.path, dllPlugins);
				event.path.loadPlugin;
				continue;
			}
		}
	};
	update(asiWatcher, true);
	update(dllWatcher, false);
}

public void loadAsi(){
	foreach (string plugin; dirEntries(getcwd, "*.asi", SpanMode.shallow))
		plugin.loadPlugin;
}

public void loadDll(eLoadType loadType = eLoadType.load){
	if ((getcwd ~ "/" ~ ini.dllLoader.path).exists && (getcwd ~ "/" ~ ini.dllLoader.path).isDir)
		foreach (string plugin; dirEntries(getcwd ~ "/" ~ ini.dllLoader.path, "*.dll", SpanMode.shallow))
			plugin.loadPlugin(loadType);
	else (getcwd ~ "/" ~ ini.dllLoader.path).mkdir;
}

public void unloadAll(){
	foreach(plugin; asiPlugins)
		plugin.libUnload;
	foreach(plugin; dllPlugins){
		plugin.callUnload;
		plugin.libUnload;
	}
}

public void loadPlugin(ref string plugin, eLoadType loadType = eLoadType.load){
	bool isDll = false;
	if (plugin.extension.asLowerCase.equal(".dll"))
		isDll = true;
		
	bool exclude = false;
	if (isDll)
		exclude = plugin.inArray(ini.dllLoader.exclude);
	else exclude = plugin.inArray(ini.asiLoader.exclude);
	
	if (!exclude){
		bool loaded = false;
		if (isDll)
			loaded = plugin.isLoaded(dllPlugins);
		else loaded = plugin.isLoaded(asiPlugins);
		
		if (!loaded){
			auto lib = plugin.libLoad;
			if (isDll){
				lib.callLoad(loadType);
				dllPlugins ~= lib;
			}
			else asiPlugins ~= lib;
		}
	}
}

public enum eLoadType : byte{
	crtMainLoad = 0,
	GetStartupInfoALoad,
	mainloopLoad,
	devCrtMainLoad,
	devGetStartupInfoALoad,
	devMainloopLoad,
	devLoad,
	load,
}

struct stLibInfo{
	string library;
	void* handle;
};

private stLibInfo libLoad(string lib){
	stLibInfo result;
	
	result.library = lib;
	
	if (!ini.devMode.enable || lib.baseName.asLowerCase.equal("sampfuncs.asi"))
		result.handle = LoadLibraryA(lib.toStringz);
	else {
		lib = tempDir.buildPath(thisProcessID.to!string~"/"~lib.baseName);
		if (!lib.dirName.exists)
			lib.dirName.mkdirRecurse;
		if (lib.exists)
			lib.remove;
		result.library.copy(lib);
		result.handle = LoadLibraryA(lib.toStringz);
	}
	
	return result;
}

private void libUnload(ref stLibInfo lib){
	FreeLibrary(lib.handle);
	if (ini.devMode.enable){
		auto devPath = tempDir.buildPath(thisProcessID.to!string~"/"~lib.library.baseName);
		if (devPath.exists)
			devPath.remove;
	}
	lib.handle = null;
}

private void libReload(ref stLibInfo lib){
	if (lib.library.extension.asLowerCase.equal(".dll"))
		lib.callUnload;
	lib.libUnload;
	lib = libLoad(lib.library);
	if (lib.library.extension.asLowerCase.equal(".dll")){
		if (ini.devMode.enable)
			lib.callLoad(eLoadType.devLoad);
		else lib.callLoad(eLoadType.load);
	}
}

private void callLoad(ref stLibInfo dll, eLoadType loadType){
	cb_load callback = cast(cb_load)GetProcAddress(dll.handle, "load");
	if (!(callback is null))
		callback(loadType);
}

private void callUnload(ref stLibInfo dll){
	cb_unload callback = cast(cb_unload)GetProcAddress(dll.handle, "unload");
	if (!(callback is null))
		callback();
}

private bool inArray(T)(ref T v, ref T[] a){
	foreach(ref T e; a)
		if (e.baseName == v.baseName)
			return true;
	return false;
}

private bool isLoaded(ref string newLib, ref stLibInfo[] libs){
	foreach(ref stLibInfo lib; libs)
		if (lib.library.baseName == newLib.baseName)
			return true;
	return false;
}
